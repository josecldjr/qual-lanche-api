/*
 * Arquivo que contém as configurações/constantes do projeto
 */

/** Porta de rede em que a aplicação será iniciada */
const PORT = process.env.PORT || 3000 

/** Palavra passe usada para criptografia */
const APP_SECRET_KEY = '#AVESTRUZ'

export {
    /** Porta de rede em que a aplicação será iniciada */
    PORT,
    /** Palavra passe usada para criptografia */
    APP_SECRET_KEY
}