import bcrypt from 'bcrypt'

/**
 * HELPER
 * Helper para senhas e criptografias
 */
export default class PasswordHelper {

    /**
     * Gera uma senha criptografada
     * @param {string} password senha
     * @returns {string} Hash da senha criptografada
     */
    static async encryptPassword(password) {
        const SALT = 10

        try {
            return await bcrypt.hash(password, salt)
        } catch (error) {
            console.log(error);
            return null
        }          
    }

    /**
     * Método que compara uma senha não-criptografada com uma criptograda
     * @param {string} password senha sem criptografia
     * @param {string} hash senha criptogradara
     * @returns {string} se a senha criptograda é igual a não-criptogradada 
     */
    static async comparePassword(password, hash) {
        return await bcrypt.compare(password, hash)
    }
}