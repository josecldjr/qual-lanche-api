import express from 'express'
import HttpStatus from 'http-status-codes' 

/*
 * ROUTES
 * roteador/controller principal
 * controlar todas as outras rotas da aplicação 
 */
 
 

// router
const router = express.Router()


/* import de constrollers */
import main from './modules/main/MainController'
import user from './modules/user/UserController'
import establishment from './modules/establishment/EstablishmentController'
import product from './modules/product/ProductController'
import evaluation from './modules/evaluation/EvaluationController'

/** rota principal */
router.use('/',  main)
router.use('/user',  user)
router.use('/establishment',  establishment)
router.use('/product',  product)
router.use('/evaluation', evaluation)

export default router

