
/**
 * DTO
 * dto para listagem de produtos
 */
export class SerachProductDTO {

    constructor (searchProductDTO) {        
        this.searchString = searchProductDTO.searchString || undefined
        this.categoryId = searchProductDTO.categoryId || undefined
        this.establishmentId = searchProductDTO.establishmentId || undefined
        this.minPrice = searchProductDTO.minPrice || undefined
        this.maxPrice = searchProductDTO.maxPrice || undefined

        this.page = searchProductDTO.page || 1
        this.maxResult = searchProductDTO.maxResult || 10

    }

    searchString

    categoryId
    establishmentId
    
    minPrice
    maxPrice

    page = 1
    maxResult = 10

}