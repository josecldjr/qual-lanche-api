import express from 'express'
import HttpStatus from 'http-status-codes'
import { ProductModel } from './model/ProductModel';
import { ProductManager } from './services/ProductManager';
import { ValidatorHelper } from '../../Helper/ValidatorHelper';
import { SerachProductDTO } from './dto/SearchProductDTO';
import verifyUser from '../../middlewares/VerifyUser';
import { EvaluationModel } from '../evaluation/model/EvaluationModel';
import { EvaluationManager } from '../evaluation/services/EvaluationManager';

/*
 * CONTROLLER
 * Controller com endpoints para Establishments
 */

const router = express.Router()

const productManager = new ProductManager()
const evaluationManager = new EvaluationManager()

/**
 * POST
 * Cria um produto e retorna o que foi criado
 */
router.post('/', async (req, res) => {

    const product = new ProductModel(req.body)

    try {
        ValidatorHelper.validateIfCampsExists(product, ['nome','descricao','id_lanchonete'])

        const retProduct = await productManager.save(product)

        res.status(HttpStatus.CREATED).json(retProduct)
    } catch (err) {        
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err)
    }
})

/**
 * POST
 * Faz uma avaliação de um produto
 */
router.post('/:id/evaluation',  verifyUser, async (req, res) => {
    
    try {
        const user = req.user
        const productId = req.params.id

        const evaluation = new EvaluationModel(req.body)

        evaluation.id_usuario = user.id_usuario
        evaluation.tipoAvaliacao = 'P'
        evaluation.id_produto = productId 

        // validação
            // se os campos foram preenchidos
        ValidatorHelper.validateIfCampsExists(evaluation, ['tipoAvaliacao','nota'])
            // se a avaliação não está ligada a mais de um objeto
        if (evaluation.id_produto && evaluation.id_lanchonete) 
            return res.status(HttpStatus.BAD_REQUEST).json({msg: 'A avaliação deve ser da lanchonete ou do produto,'})

        const ret = await evaluationManager.save(evaluation)

        res.status(HttpStatus.OK).json(ret)
    } catch (err) {
        console.log(err)        
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err)
    }
})

/**
 * GET
 * Faz uma listagem de produtos
 */
router.get('/', async (req, res) => { 

    const searchDTO =  new SerachProductDTO(req.query)

    try {
        
        const ret = await productManager.find(searchDTO)

        res.status(HttpStatus.OK).json(ret)

    } catch (err) {        
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err)
    }
})

/**
 * GET
 * Listagem de categorias
 */
router.get('/categories', async (req, res) => {
    
    try {
        const ret = await productManager.findCategories()

        res.status(HttpStatus.OK).json(ret)
    } catch (err) {
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err)
    }
})

/**
 * DELETE
 * Deleta um produto à partir do id
 */
router.delete('/:id', async (req, res) => {

    try {
        const id = req.params.id

        await productManager.deleteById(id)

        res.status(HttpStatus.NO_CONTENT).end()
    } catch (err) {
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err)
    }

})

/**
 * PUT
 * Altera um produto pelo id
 */
router.put('/:id', async (req, res) => {

    try {
        const productId = req.params.id
        const productParams = req.body

        const upatedProduct = await productManager.updateById(productId, productParams)

        res.status(HttpStatus.OK).json(upatedProduct)
    } catch (err) {
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err)
    }
})


module.exports = router