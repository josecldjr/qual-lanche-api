/**
 * MODEL
 * Model para categoria
 */
export class CategoryModel {

    constructor(category) {
        this.id_categoria = category.id_categoria
        this.nome = category.nome
        this.descricao = category.descricao
    }
    
    id_categoria
    nome
    descricao
}