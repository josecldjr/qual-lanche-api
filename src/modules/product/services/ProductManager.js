import { ProductDAO } from './ProductDAO'
import { CategoryDAO } from './CategoryDAO';

/**
 * MANAGER
 * Responsável por gerenciar operações relacionadas a produto
 */
export class ProductManager {

    productDAO = new ProductDAO()
    categoryDAO = new CategoryDAO()

    constructor() {
        this.productDAO = new ProductDAO()
    }

    /**
     * Cria um produto 
     * @param {*} product dados/model do produto
     */
    async save(product) {
        return await this.productDAO.save(product)
    }

    /**
     * Retorna um produto pelo id
     */
    async findById(id, relations) {
        return await this.productDAO.findById(id, relations)
    }

    /**
     * Faz uma listagem de produtos
     * @param {SearchProductDTO} searchProductDTO 
     */
    async find(searchProductDTO) {
       return await this.productDAO.find(searchProductDTO)
    }

    /**
     * Retorna as categorias
     */
    async findCategories() {
        return await this.categoryDAO.find()
    }

    /**
     * Deleta um produto pelo id
     */
    async deleteById(id) {
        return await this.productDAO.deleteById(id)
    }

    /**
     * Atualiza um produto pelo id
     * @param id id do produto que será atualizado
     * @param product objeto contendo todos os campos que serão atualziados
     */
    async updateById(id, product) {
        console.log('manager');
        
        return await this.productDAO.updateById(id, product)
    }
}