import conn from '../../../connection'
import CategoryModel from '../model/CategoryModel'

/**
 * DAO
 * Dao para categorias
 */
export class CategoryDAO {

    /**
     * Encontra uma categoria pelo id
     * @param {number} id 
     */
    async findById(id) {
        let sql = `SELECT * FROM categoria WHERE id_categoria = ${id} LIMIT 1`

        return new Promise((resolve, reject) => {
            conn.query(sql, (err, result) => {
                if (err) return reject(err)

                let ret = result.length > 0 ? (result[0]) : undefined

                return resolve(ret)
            })
        })
    }

    /**
     * Retorna uma lista de categorias para produtos
     */
    async find() {
        let sql = 'SELECT * FROM categoria'

        return new Promise((resolve, reject) => {
            conn.query(sql, (err, result) => {
                if (err) return reject(err)

                return resolve({
                    list: result
                })
            })
        })
    }


    /**
     * Retorna um alista de categorias
     */
    list() {
        let sql = `SELECT * FROM categoria`

        return new Promise((resolve, reject) => {
            conn.query(sql, (err, result) => {
                if (err) return reject(err)

                return resolve(result)
            })
        })
    }
}