import conn from '../../../connection'
import { ProductModel } from '../model/ProductModel'
import {EstablishmentDAO} from '../../establishment/services/EstablishmentDAO'
import { CategoryDAO } from './CategoryDAO';
import {QueryBuilder} from 'liteqbjs'

const qb = new QueryBuilder()
/**
 * DAO
 * Gerencia operações no banco de dados relacionadas a produto
 */
export class ProductDAO {
    TABLE = 'produto'

    establishmentDAO
    categoryDao = new CategoryDAO()

    constructor() {
        this.establishmentDAO = new EstablishmentDAO()
    }

    /**
     * Cria um produto no banco de dados
     * @param {*} product 
     */
    async save (product) {
        let sql = `
            INSERT INTO produto
            (
                nome,
                descricao,
                preco,
                id_categoria,
                id_lanchonete
            ) VALUES         
        ` 

        sql += '(\'' + product.nome + '\','
        sql += '\'' + product.descricao + '\','
        sql += '\'' + product.preco + '\','
        sql += '\'' + product.id_categoria + '\','
        sql += '\'' + product.id_lanchonete + '\')'
 
        return new Promise( async (resolve, reject) => {

            try {
                conn.query(sql, async (err, result) => {
                    if (err) return reject(err)
                    
                    let ret = await this.findById(result.insertId)
                    
                    resolve(ret)
                })

            } catch (e) {
                reject(e)
            }

        })
        
    }

    /**
     * Encontra um produto pelo id
     * @param {number} id id do produto
     * @param {boolean} relations popular relações?
     */
    async findById(id, relations = true) {
        let sql = `SELECT * FROM produto WHERE id_produto = ${id}`
    
        return new Promise((resolve, reject) => {

            try {

                conn.query(sql, async (err, result) => {
                    if (err) return reject(err)

                    let ret = result.length > 0 ? new ProductModel(result[0]) : undefined
 
                    if (ret) {
                        ret.lanchonete = relations ? await this.establishmentDAO.findById(ret.id_lanchonete) : undefined
                    
                    }

                    resolve(ret)
                })

            } catch (e) {
                reject(e)
            }

        })
    
    }

    /**
     * Faz uma listagem de produtos
     * @param {SearchProdctDTO} searchProdctDTO informações para a busca
     */
    async find(searchProdctDTO) {
        let sql = `SELECT * FROM produto WHERE
                    nome LIKE '%${searchProdctDTO.searchString || ''}%'
                    AND descricao LIKE '%${searchProdctDTO.searchString || ''}%'`

        if (searchProdctDTO.categoryId) {
            sql += ` AND id_categoria = ${searchProdctDTO.categoryId}`
        }

        if (searchProdctDTO.establishmentId) {
            sql += ` AND id_lanchonete = ${searchProdctDTO.establishmentId}`
        }

        if (searchProdctDTO.minPrice) {
            sql += ` AND preco > ${searchProdctDTO.minPrice}`
        }

        if (searchProdctDTO.maxPrice) {
            sql += ` AND preco < ${searchProdctDTO.maxPrice}`
        }

        sql += ' ORDER BY preco ASC'
        sql += ` LIMIT ${searchProdctDTO.maxResult} OFFSET ${(searchProdctDTO.page - 1) * searchProdctDTO.maxResult}`
 
        return new Promise((resolve, reject) => {
            conn.query(sql,async (err, result) => {
                if (err) return reject(err)

                let retArray = result.map(async (product) =>{
                    product.categoria = await this.categoryDao.findById(product.id_categoria)
                    // product.categoria = 'asda'

                    return product
                })
                
                return Promise.all(retArray).then((result) => { 
                    return resolve({
                        list: result
                    }) 
                })
            })
        })
    }

    /**
     * Deleta um produto a partir do id
     * @param {*} id 
     */
    async deleteById(id) {
        let sql = qb.delete(this.TABLE, `id_produto = ${id}`)

        return new Promise((resolve, reject) => {
            conn.query(sql, (err, result) => {
                if (err) return reject(err)

                resolve()
            })
        })
    }

    /**
     * Atualiza um produto no banco pelo id
     * @param id id do produto
     * @param product obj com todos os parâmetros que serão alterados
     */
    async updateById(id, product) {
        console.log('dao');
        
        if (product.id_produto) throw {msg: 'o campo de id não pode ser alterado'}

        let sql = qb.update(this.TABLE, `id_produto = ${id}`, product, 1)
        
        return new Promise((resolve, reject) => {
            conn.query(sql, async (err, result) => {
                if (err) return reject(err)
                
                try {
                    console.log(sql);
                    let retProduct = await this.findById(id, true)

                    return resolve(retProduct)
                } catch(err) { console.log(err);
                    reject(err)
                }

                
            })
        })
    }
}