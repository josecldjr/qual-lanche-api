import express from 'express'
import HttpStatus from 'http-status-codes'

/*
 * ROUTES
 * roteador/controller principal
 * controlar todas as outras rotas da aplicação 
 */

const router = express.Router()

/** ping */
router.get('/', (req, res) => {
     
    res
    .status(HttpStatus.OK)
    .send('<h1>Tá funcionando... pode ficar tranquio!</h1>')
})


export default router

