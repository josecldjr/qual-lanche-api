

/**
 * MODEL
 * Model para usuário
 */
export class UserModel {

    constructor(user) {
        if (!user) return
        this.id_usuario = user.id_usuario || null
        this.nome = user.nome
        this.data_nascimento = user.data_nascimento
        this.sexo = user.sexo
        this.email = user.email
        this.uid_firebase = user.uid_firebase
        this.perfil_lanchonete = user.perfil_lanchonete
    }

    id_usuario
    nome
    data_nascimento
    sexo
    email
    uid_firebase
    perfil_lanchonete
}