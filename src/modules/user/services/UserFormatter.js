import { UserModel } from "../model/UserModel";
import { DateUtils } from "../../../common/utils/DateUtils";

/**
 * FORMATTER
 * Responsável por formatar dados referentes a user
 */
export class UserFormatter {

    static formatUserForLoginReturn(user) {
        let ret = new UserModel()
        
        ret.id_usuario = user.id_usuario
        ret.data_nascimento = DateUtils.formatToYYYYMMDD(user.data_nascimento)
        ret.email = user.email
        ret.perfil_lanchonete = user.perfil_lanchonete
        ret.sexo = user.sexo

        return ret
    }
}