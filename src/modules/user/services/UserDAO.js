import { UserModel } from '../model/UserModel';
import conn from '../../../connection'
import { DateUtils } from '../../../common/utils/DateUtils';

export class UserDAO {
    
    /**
     * Cria um usuário no banco
     * @param {*} user 
     */
    async save(user) {
        if (!user.id_usuario) {
            let sql = `INSERT INTO usuario
            (
                nome,
                data_nascimento,
                sexo,
                email,
                uid_firebase,
                perfil_lanchonete,
                senha
            ) 
            VALUES `
            
            // adiciona os campos para a query
            sql += '(\'' + user.nome + '\','
            sql += '\'' + user.data_nacimento + '\','
            sql += '\'' + user.sexo + '\','
            sql += '\'' + user.email + '\','
            sql += '\'' + user.uid_firebase + '\','
            sql += '\'' + user.perfil_lanchonete + '\','
            sql += '\'' + user.senha + '\')'
            
            return new Promise((resolve, reject) => {                
                try {
                    conn.query(sql, async (err, result) => {
                        if (err) return reject({msg: err})                        
                    
                        const retUser = await this.findById(result.insertId)             
                        
                        resolve (retUser)
                    })
                } catch (e) {
                    reject(e)
                }
            })
           
        }
    }

    /**
     * Retorna um usuário pelo id
     * @param {*} id 
     */
    async findById(id) {
        let sql = `
            SELECT * FROM usuario WHERE id_usuario = '${id}'
        `
    
        return new Promise((resolve, reject) => {
             
            try {
                conn.query(sql, (err, result) => {                    
                    if (err) return reject({msg: err})
                                
                    const ret = result.length > 0 ? new UserModel(result[0]) : undefined
                  
                    resolve (ret)
                })
            } catch (e) {
                reject(e)
            }
        }) 

    }

    /**
     * Encontra um usuário pelos filtros especificados
     * @param {*} filters 
     */
    async findOneByFilters(filters) {

        let sql = `
            SELECT * FROM usuario 
        `

        const keys = Object.keys(filters)
        const values = Object.values(filters)

        if (keys.length > 0) {
            sql += ' WHERE '
            
            keys.forEach((key, i, keys) => {
                sql += ` ${key} = '${values[i]}' `
            
                if ( i + 1 < keys.length ){
                    sql += ' AND '
                }
            })
        }

        sql += ' LIMIT 1 '
     
        
        return new Promise((resolve, reject) => {
            try {
                conn.query(sql, (err, result) => {
                    if (err) return reject(err)

                    const ret = result.length > 0 ? result[0] : undefined
                    
                    
                    resolve(ret)
                })
 
            } catch (e) {
                reject(e)
            }
        }) 
        
    }
} 