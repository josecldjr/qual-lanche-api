
/**
 * MODEL
 * Model para avaliações de produto e lanchonete
 */
export class EvaluationModel{

    constructor(evaluation) {
        this.tipoAvaliacao = evaluation.tipoAvaliacao
        this.nota = evaluation.nota
        this.comentario = evaluation.comentario
        this.id_usuario = evaluation.id_usuario
        this.id_produto = evaluation.id_produto
        this.id_lanchonete = evaluation.id_lanchonete
        this.usuario = evaluation.usuario
        this.lanchonete = evaluation.lanchonete
        this.produto = evaluation.produto
    }

    id_avaliacao = undefined
    
    tipoAvaliacao = undefined
    nota = undefined
    comentario = undefined

    id_usuario = undefined
    id_produto = undefined
    id_lanchonete = undefined

    usuario = undefined
    lanchonete = undefined
    produto = undefined
}