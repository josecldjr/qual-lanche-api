import express from 'express'
import HttpStatus from 'http-status-codes'
import AccessTokenHelper from '../../Helper/AccessTokenHelper'
import { EstablishmentModel } from './model/EvaluationModel'
import { EvaluationManager } from './services/EvaluationManager';


/**
 * CONTROLLER
 * Controller para avaliações/comentários
 */

const evaluationManager = new EvaluationManager()
const router = express.Router()

/**
 * DELETE 
 * Deleta uma avaliação no sistema
 * TODO: fazer validação de suuário
 */
router.delete('/:id', (req, res) => {
    const id = parseInt(req.params.id)
    console.log(id);
    
    try {
        evaluationManager.deleteById(id)

        res.status(HttpStatus.NO_CONTENT).end()
    } catch (err) {
        console.log(err)
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({msg:err})
    }
    
})


module.exports = router