import { EvaluationDAO } from "./EvaluationDAO";

/**
 * MANAGER
 * Gerencia as operações relacionadas a avaliações de produto e lanchonete
 */
export class EvaluationManager {
    
    evaluationDAO = new EvaluationDAO()

    /**
     * Cria uma avaliação no banco
     * @param {number} evaluation 
     */
    async save(evaluation) {
       return await this.evaluationDAO.save(evaluation)
    }

    /**
     * Deleta a uma avaliação a partir do id
     */
    async deleteById(id) {
        return await this.evaluationDAO.deleteById(id)
    }
}