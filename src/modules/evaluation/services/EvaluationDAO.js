import {QueryBuilder} from 'liteqbjs'
import { EvaluationModel } from '../model/EvaluationModel';
import conn from '../../../connection'

const qb = new QueryBuilder()

/**
 * DAO
 * Dao para avaliações de produtos e lanchonetes
 */
export class EvaluationDAO {
    TABLE = 'avaliacao'

    /**
     * Cria uma avaliação
     */
    async save(evaluation) {
        let inputEv = new EvaluationModel(evaluation)

        Object.keys(inputEv).forEach((k) => { 
            if (inputEv[k] === undefined) {
                delete inputEv[k]
            }
         })
        
        let sql = qb.insert(this.TABLE, inputEv)

        return new Promise((resolve, reject) => {
            conn.query(sql, async (err, result) => {
                if (err) return reject(err) 
                
                let ret = await this.findById(result.insertId)
                console.log(result);
                
                return resolve(ret)
            })
        })
    }

    /**
     * Busca uma avaliação pelo id
     * @param {number} id 
     */
    findById(id) {
        let sql = qb.select(this.TABLE, {where: `id_avaliacao = ${id}`})

        return new Promise((resolve, reject) => {
            conn.query(sql, (err, result) => {
                if (err) return reject(err)
                
                let ret = result.length > 0 ? result[0] : undefined 
                
                return resolve(ret)
            })
        }) 
    }

    /**
     * Deleta uma avaliação
     */
    deleteById(id) {
        let sql = qb.delete(this.TABLE, `id_avaliacao = ${id}`)

        return new Promise((resolve, reject) => {
            conn.query(sql, (err, result) => {
                if (err) return reject(err)

                console.log(result);
                

                resolve()
            })
        })
    }
}