

/**
 * DTO
 * Dto para busca de lanchoenetes
 */
export class SearchEstablishmentDTO {

    constructor(searchDTO) {
        this.searchString = searchDTO.searchString || undefined
        this.address = searchDTO.address || undefined
        this.city = searchDTO.city || undefined
        this.state = searchDTO.state || undefined
        this.idFormaPagamento = searchDTO.idFormaPagamento || undefined

        this.page = searchDTO.page || 1
        this.maxResults = searchDTO.maxResults || 10
    }

    searchString = undefined
    address = undefined
    city = undefined
    state = undefined
    idFormaPagamento = undefined

    page = undefined
    maxResults = undefined

}