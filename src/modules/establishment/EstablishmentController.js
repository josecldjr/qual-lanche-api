import express from 'express'
import HttpStatus from 'http-status-codes'
import AccessTokenHelper from '../../Helper/AccessTokenHelper'
import {EstablishmentManager} from './services/EstablishmentManager'
import { EstablishmentModel } from './model/EstablishmentModel'
import {SearchEstablishmentDTO} from './dto/SearchEstablishmentDTO'
import { EvaluationModel } from '../evaluation/model/EvaluationModel';
import { EvaluationManager } from '../evaluation/services/EvaluationManager';
import {ValidatorHelper} from '../../Helper/ValidatorHelper'
import verifyUser from '../../middlewares/VerifyUser'
/*
 * CONTROLLER
 * Controller com endpoints para Establishments
 */

const router = express.Router()
 
const establishmentManager =  new EstablishmentManager()
const evaluationManager = new EvaluationManager()
/**
 * Cria uma lanchonete
 */
router.post('/', async (req, res) => {

    const establishment = req.body

    try {
        const createdEstablishment = await establishmentManager.save(establishment)

        res.status(HttpStatus.CREATED).json(createdEstablishment)

    } catch (err) {
        console.log(err)        
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({msg:err})
    }

})

/**
 * Retorna as formas de pagamento deisponíveis
 */
router.get('/payment-methods', async (req, res) => {
    try {
        const paymentMethods = await establishmentManager.listPaymentMethods()

        res.status(HttpStatus.OK).json(paymentMethods)
    } catch (err) {
        console.log(err)
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({msg:err})        
    }
})

/**
 * Buscar uma lanchonete pelo id
 */
router.get('/:id',async (req, res) => {
    
    try {
        const id = parseInt(req.params.id)

        const establishment = await establishmentManager.findById(id)

        if (!establishment) res.status(HttpStatus.NOT_FOUND).json({msg: 'Lanchonete não encontrada'})

        res.status(HttpStatus.OK).json(establishment)
    } catch (err) {
        console.log(err)
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({msg:err})        
    }
})

/**
 * Faz uma avaliação de um produto
 */
router.post('/:id/evaluation', verifyUser, async (req, res) => {
    const user = req.user
    const establishmentId = req.params.id

    try {
        const evaluation = (req.body)

        evaluation.id_usuario = user.id_usuario
        evaluation.tipoAvaliacao = 'L'
        evaluation.id_lanchonete = establishmentId 

        // validação
        ValidatorHelper.validateIfCampsExists(evaluation, ['tipoAvaliacao','nota'])
        if (evaluation.id_produto && evaluation.id_lanchonete) 
            return res.status(HttpStatus.BAD_REQUEST).json({msg: 'A avaliação deve ser da lanchonete ou do produto,'})

        const ret = await  evaluationManager.save(evaluation)

        res.status(HttpStatus.CREATED).json(ret)
    } catch (err) {
        console.log(err)
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({msg:err})
    }
})

/**
 * Busca uma lista de lanchonetes
 */
router.get('/', async (req, res) => {
    const searchDTO = new SearchEstablishmentDTO(req.query)

    try {
        const establishments = await establishmentManager.find(searchDTO)

        res.status(HttpStatus.OK).json(establishments)
    } catch (err){
        console.log(err)
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({msg:err})
    }
})

/**
 * DELETE
 * deleta uma lanchonete pelo id
 */
router.delete('/:id', async (req, res) => {

    try {
        const id = req.params.id

        await establishmentManager.deleteById(id)

        res.status(HttpStatus.NO_CONTENT).end()
    } catch (err) {
        console.log(err)
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({msg:err})
    }
})


module.exports = router