import HttpStatus from 'http-status-codes'
import AccessTokenHelper from '../Helper/AccessTokenHelper';
import { UserManager } from '../modules/user/services/UserManager';

// services
const userManager = new UserManager()

/**
 * Função para verificação e validação de usuário
 */
async function verifyUser(req, res, next) {
    let tokenInfo

    // checa se o usuário está autenticado
    if (!req.headers['authorization']){        
        res.status(HttpStatus.UNAUTHORIZED).json({message: "Usuário não autenticado"})
        return
    }

    try {
        let token = AccessTokenHelper.getBearerFromHeaders(req)

        tokenInfo = await AccessTokenHelper.readToken(token)
    } catch (e) {
        res.status(HttpStatus.UNAUTHORIZED).json({message: "Usuário não autenticado"})
        return
    }
    
    // busca o usuário no banco
    const user = await userManager.findById(tokenInfo.id)  

    // caso o usuário não exista
    if (!user) {        
        res.status(HttpStatus.UNAUTHORIZED).json({message: "Usuário inválido ou não existe"})
        return
    }
    
    // passa informação do usuário para o banco
    req.user = user
    
    next()
}


export default verifyUser