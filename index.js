const cmd = require('child_process').exec

try {
    const exec = cmd('npm install && npm run build && npm run start')

    exec.stdout.on('data', data => {
        console.log( `stdout: ${data}`)
    })

} catch (err) {
    console.log('Starting error: ', err)
}