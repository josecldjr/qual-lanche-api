-- phpMyAdmin SQL Dump
-- version 4.3.7
-- http://www.phpmyadmin.net
--
-- Host: mysql10-farm76.kinghost.net
-- Tempo de geração: 10/05/2019 às 14:22
-- Versão do servidor: 5.6.36-log
-- Versão do PHP: 5.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `quallanche`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `avaliacao`
--

CREATE TABLE IF NOT EXISTS `avaliacao` (
  `id_avaliacao` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_produto` int(11) NULL,
  `id_lanchonete` int(11) NULL,
  `tipoAvaliacao` enum('P','L') NOT NULL,
  `nota` smallint(6) NOT NULL,
  `comentario` varchar(300) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `avaliacao`
--

INSERT INTO `avaliacao` (`id_avaliacao`, `id_usuario`, `id_produto`, `id_lanchonete`, `tipoAvaliacao`, `nota`, `comentario`) VALUES
(1, 3, 3, 1, 'P', 3, 'O suco estava quente'),
(2, 4, 3, 2, 'P', 4, 'Suco estava bom'),
(3, 4, 3, 2, 'L', 2, 'Lanchonete suja');

-- --------------------------------------------------------

--
-- Estrutura para tabela `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
  `id_categoria` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `descricao` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `nome`, `descricao`) VALUES
(1, 'Salgado', NULL),
(2, 'Bebida', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `formapagamento`
--

CREATE TABLE IF NOT EXISTS `formapagamento` (
  `id_formaPagamento` int(11) NOT NULL,
  `formaPagamento` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `formapagamento`
--

INSERT INTO `formapagamento` (`id_formaPagamento`, `formaPagamento`) VALUES
(1, 'Dinheiro'),
(2, 'Dinheiro e Crédito'),
(3, 'Dinheiro e Débito'),
(4, 'Dinheiro, Crédito e Débito');

-- --------------------------------------------------------

--
-- Estrutura para tabela `lanchonete`
--

CREATE TABLE IF NOT EXISTS `lanchonete` (
  `id_lanchonete` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `telefone01` varchar(20) DEFAULT NULL,
  `telefone02` varchar(20) DEFAULT NULL,
  `cep` varchar(10) DEFAULT NULL,
  `endereco` varchar(50) NOT NULL,
  `cidade` varchar(30) NOT NULL,
  `estado` varchar(2) NOT NULL,
  `id_formaPagamento` int(11) DEFAULT NULL,
  `id_usuario` int(11) NOT NULL,
  `latitude` decimal(10,8) DEFAULT NULL,
  `longitude` decimal(11,8) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `lanchonete`
--

INSERT INTO `lanchonete` (`id_lanchonete`, `nome`, `telefone01`, `telefone02`, `cep`, `endereco`, `cidade`, `estado`, `id_formaPagamento`, `id_usuario`, `latitude`, `longitude`) VALUES
(1, 'BAR DO ZÉ', '(31) 3636-3636', NULL, '32109-321', 'RUA GOITACAZES, Nº 1500, BAIRRO: BARRO PRETO', 'Belo Horizonte', 'MG', 1, 1, NULL, NULL),
(2, 'Super Burrito', '(31) 7535-7636', '(31) 7647-2457', '54908-327', 'RUA GOITACAZES, Nº 1527, BAIRRO: BARRO PRETO', 'BELO HORIZONTE', 'MG', 1, 2, NULL, NULL),
(29, 'Prodrão da esquina', '3199999999', '3199999999', '3113555', 'apsdakosmdokasmdko', 'Neves', 'MG', 1, 53, '9.90000000', '9.90000000'),
(41, 'Prodrão da esquina', '3199999999', '3199999999', '3113555', 'apsdakosmdokasm312321dko', 'Neves', 'MG', 3, 2, '9.90000000', '9.90000000'),
(42, 'Prodrão da esquina', '3199999999', '3199999999', '3113555', 'apsdakosmdokasm312321dko', 'Neves', 'MG', 3, 2, '9.90000000', '9.90000000');

-- --------------------------------------------------------

--
-- Estrutura para tabela `produto`
--

CREATE TABLE IF NOT EXISTS `produto` (
  `id_produto` int(11) NOT NULL,
  `nome` varchar(80) NOT NULL,
  `descricao` varchar(250) DEFAULT NULL,
  `preco` decimal(10,2) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `id_lanchonete` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `produto`
--

INSERT INTO `produto` (`id_produto`, `nome`, `descricao`, `preco`, `id_categoria`, `id_lanchonete`) VALUES
(1, 'Coxinha', 'Coxinha recheada de frango com catupiri', '2.50', 1, 1),
(2, 'Pão de queijo', 'Pão de queijo tradicional', '1.50', 1, 1),
(3, 'Suco de laranja', 'Suco natural', '2.50', 2, 1),
(4, 'Coxinha de Frango c/ Catupiri', NULL, '2.50', 1, 2),
(5, 'Pão de queijo', NULL, '1.60', 1, 2),
(6, 'Suco natural de laranja', 'Suco natural', '2.30', 2, 2);

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nome` varchar(80) NOT NULL,
  `data_nascimento` date DEFAULT NULL,
  `sexo` enum('M','F') DEFAULT NULL,
  `email` varchar(80) NOT NULL,
  `uid_firebase` varchar(26) NOT NULL,
  `perfil_lanchonete` int(11) NOT NULL,
  `senha` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nome`, `data_nascimento`, `sexo`, `email`, `uid_firebase`, `perfil_lanchonete`, `senha`) VALUES
(1, 'Zé', '1970-05-02', 'M', 'zedobar@yahoo.com', 'zezinho123', 1, '12345'),
(2, 'João', '1994-10-02', 'M', 'joaoninguem@yahoo.com', 'joao123', 1, '12345'),
(3, 'Maria', '1996-02-10', 'F', 'maria@yahoo.com.br', 'josemaria123', 0, '12345'),
(4, 'André', '1980-07-30', 'M', 'andreandrade@gmail.com', 'andre123', 0, '12345'),
(53, 'asdadsasd', '2019-05-22', 'M', 'dsada@asdas.cosakd', '21312', 1, '12345'),
(72, 'asdadsasd', '2019-05-22', 'M', 'dsada@aasdsdas.cosakd', '21312', 1, '12345'),
(74, 'asdadsasd', '2019-05-22', 'M', 'dsada@aasdssdas.cosakd', '21312', 1, '12345');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `avaliacao`
--
ALTER TABLE `avaliacao`
  ADD PRIMARY KEY (`id_avaliacao`,`tipoAvaliacao`,`id_usuario`,`id_produto`,`id_lanchonete`), ADD KEY `id_usuario` (`id_usuario`), ADD KEY `id_lanchonete` (`id_lanchonete`), ADD KEY `id_produto` (`id_produto`);

--
-- Índices de tabela `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Índices de tabela `formapagamento`
--
ALTER TABLE `formapagamento`
  ADD PRIMARY KEY (`id_formaPagamento`);

--
-- Índices de tabela `lanchonete`
--
ALTER TABLE `lanchonete`
  ADD PRIMARY KEY (`id_lanchonete`), ADD KEY `id_formaPagamento` (`id_formaPagamento`), ADD KEY `id_usuario` (`id_usuario`);

--
-- Índices de tabela `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`id_produto`), ADD KEY `id_categoria` (`id_categoria`), ADD KEY `id_lanchonete` (`id_lanchonete`);

--
-- Índices de tabela `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `avaliacao`
--
ALTER TABLE `avaliacao`
  MODIFY `id_avaliacao` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `formapagamento`
--
ALTER TABLE `formapagamento`
  MODIFY `id_formaPagamento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de tabela `lanchonete`
--
ALTER TABLE `lanchonete`
  MODIFY `id_lanchonete` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT de tabela `produto`
--
ALTER TABLE `produto`
  MODIFY `id_produto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de tabela `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=80;
--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `avaliacao`
--
ALTER TABLE `avaliacao`
ADD CONSTRAINT `avaliacao_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`),
ADD CONSTRAINT `avaliacao_ibfk_2` FOREIGN KEY (`id_lanchonete`) REFERENCES `lanchonete` (`id_lanchonete`),
ADD CONSTRAINT `avaliacao_ibfk_3` FOREIGN KEY (`id_produto`) REFERENCES `produto` (`id_produto`);

--
-- Restrições para tabelas `lanchonete`
--
ALTER TABLE `lanchonete`
ADD CONSTRAINT `lanchonete_ibfk_1` FOREIGN KEY (`id_formaPagamento`) REFERENCES `formapagamento` (`id_formaPagamento`),
ADD CONSTRAINT `lanchonete_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`);

--
-- Restrições para tabelas `produto`
--
ALTER TABLE `produto`
ADD CONSTRAINT `produto_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`),
ADD CONSTRAINT `produto_ibfk_2` FOREIGN KEY (`id_lanchonete`) REFERENCES `lanchonete` (`id_lanchonete`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
