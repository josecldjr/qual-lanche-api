﻿URL BASE: https://qual-lanche-api.herokuapp.com
URL BASE: http://localhost:3000 (caso esteja usando a api local)


Criar estabececimento
POST /establishment
{
	"nome": String, 
	"telefone01": String,
	"telefone02": String,
	"cep": String,
	"endereco": String,
	"cidade": String,
	"estado": String,
	"id_formaPagamento": Number,
	"id_usuario": Number,
	"latitude": Decimal,
	"longitude": Decimal
}

Criar estabelecimento como  (necessário estar logado para realizar essa acção)
POST /user/create-establishment
{
	"nome": String,
	"telefone01": String,
	"telefone02": String,
	"cep": String,
	"endereco": String,
	"cidade": String,
	"estado": String,
	"id_formaPagamento": Number,
	"id_usuario": Number,
	"latitude": Decimal,
	"longitude": Decimal
}

Criar um usuário
POST /user
{
	"nome": String,
	"data_nacimento": Timestamt(YYYY-MM-DD),
	"sexo": Char(1),
	"email": String,
	"uid_firebase": String,
	"senha": String,
	"perfil_lanchonete": Number ( 0 ou 1 )
}

Fazer login como usuário (usuário já deve existir no banco)
POT /user/login
{
	"email": String,
	"senha": String
}

Retorna as formas de pagamento
GET /establishment/payment-methods=
{

}

Cria um produto
POST /product
{
	"nome": String,
	"descricao": String,
	"preco": Number,
	"id_categoria": Number,
	"id_lanchonete": Number
}

Retorna uma lista de produtos
GET /product
{
	searchString: String,
	minPrice: Number,
	maxPrice: Number,
	establishmentId: Number,
	categoryId: Number,
	page: Number,
	maxResult: Number
}

GET /product/categories
{
	
}

GET /establishment/:id
{
	
}

POST /establishment/:id/evaluation
{
	"nota": Number,
	"comentario": String
}

POST /product/:id/evaluation
{
	"nota": Number,
	"comentario": String
}

DELETE evaluation/:id 
{
	
}

DELETE establishment/:id
{
	
}

PUT product 
{
	"nome": String,
	"descricao": String,
	"preco": Number,
	"id_categoria": Number,
	"id_lanchonete": Number
}